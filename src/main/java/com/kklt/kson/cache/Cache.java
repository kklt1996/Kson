package com.kklt.kson.cache;

import com.dyuproject.protostuff.Schema;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 缓存运行时生成的Schema
 * @author lishouyu
 */
public class Cache {

  private static LoadingCache<Class<?>,Object> cachedSchema = CacheBuilder.<Class<?>>newBuilder()
      .expireAfterWrite(8, TimeUnit.DAYS)
      .recordStats()
      .initialCapacity(1<<4)
      .build(new CacheLoader<Class<?>, Object>() {
        @Override
        public Object load(Class<?> s) throws Exception {
          return Null.NULL;
        }
      });


  public static <T> void put(Class<?> key,Schema<T> value){
    cachedSchema.put(key,value);
  }

  @SuppressWarnings("unchecked")
  public static  Object get(Class<?> key){
    Object value = null;
    try {
      value = cachedSchema.get(key);
      if (value == Null.NULL){
        return null;
      }
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return value;
  }

}
