package com.kklt.kson.annotation;

import java.lang.annotation.*;

/**
 * 与生成Schema
 * @author lishouyu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface KsonScan {
  String[] basePackages();
}
