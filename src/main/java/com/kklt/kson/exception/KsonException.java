package com.kklt.kson.exception;

/**
 * Kson异常
 * @author lishouyu
 */
public class KsonException extends RuntimeException {
  public KsonException(String message) {
    super(message);
  }
}
