package com.kklt.kson.entity;


import java.io.*;

public class User implements Serializable{

  private transient String name;

  private String user;

  private  Class cls;

  public Class getCls() {
    return cls;
  }

  public void setCls(Class cls) {
    this.cls = cls;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }
  // 替换原有的序列化对象,对代理对象进行序列化
/*  private Object writeReplace() throws ObjectStreamException {
    System.out.println("writeReplace");
    return null;
  }*/

  /**
   * 自定义jdk序列化
   * @param out
   * @throws IOException
   */
  private void writeObject(ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
    // 进行自定义的序列化
    out.writeObject(this.cls.getId());
    out.writeObject(this.cls.getName());
  }

  /**
   * 自定义jdk的反序列化
   * @param in
   * @throws IOException
   * @throws ClassNotFoundException
   */
  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    // 进行参数的校验
    // 自定义的赋值
    String clsId = (String)in.readObject();
    String clsName = (String)in.readObject();
    Class aClass = new Class();
    aClass.setId(clsId);
    aClass.setName(clsName);
    this.cls = aClass;
  }

  /**
   * 可在单例子模式下返回单例的值
   * @return
   */
//  private Object readResolve(){
//    return null;
//  }

  @Override
  public String toString() {
    return "User{" +
        "name='" + name + '\'' +
        ", user='" + user + '\'' +
        ", cls=" + cls +
        '}';
  }
}
