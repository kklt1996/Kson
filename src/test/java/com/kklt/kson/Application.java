package com.kklt.kson;

import com.kklt.kson.annotation.KsonScan;
import com.kklt.kson.entity.Class;
import com.kklt.kson.entity.User;

import java.io.*;

@KsonScan(basePackages = {"com.kklt.kson.entity"})
public class Application {

  public static void main(String[] args) throws IOException {

    // 预加载Schema
    Kson.init(Application.class);
    User user = new User();
    user.setName("name");
    user.setUser("user");
    Class aClass = new Class();
    aClass.setName("一年级一班");
    aClass.setId("1");
    user.setCls(aClass);

    // Kson序列化
    long l = System.currentTimeMillis();
    for (int i = 0; i < 10000 * 1000; i++) {
      byte[] serialize = Kson.serialize(user);
      User deserialize = Kson.deserialize(serialize, user.getClass());
    }
    long l1 = System.currentTimeMillis();
    System.out.println(l1-l);

    // jdk自带序列化
    long l2 = System.currentTimeMillis();
    for (int i = 0; i < 10000*1000; i++) {
      ObjectOutputStream objectOutputStream = null;
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
      try {
        objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(user);
      } catch (IOException e) {
        e.printStackTrace();
      }
      finally {
        if (objectOutputStream!=null){
          objectOutputStream.close();
        }
      }
      ObjectInputStream objectInputStream = null;
      try {
        objectInputStream = new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        User object = (User)objectInputStream.readObject();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } finally {
        if (objectOutputStream!=null){
          objectInputStream.close();
        }
      }
    }
    long l3 = System.currentTimeMillis();
    System.out.println(l3-l2);
  }
}
