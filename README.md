Kson是对protostuff进行了封装,方便使用protostuff进行序列化和反序列化.提供了Schema预加载,Schema缓存等功能.

#### 使用方式
定义要进行序列化的类
```java
package com.kklt.kson.entity;

public class User {
  
  // 不进行序列化
  private transient String name;

  private String user;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }
}
```
#### 使用Kson进行序列化和反序列化
```java
package com.kklt.kson;

import com.kklt.kson.annotation.KsonScan;
import com.kklt.kson.entity.User;

// 预加载Schema
@KsonScan(basePackages = {"com.kklt.kson.entity"})
public class Application {

  public static void main(String[] args) {
    Kson.init(Application.class);
    User user = new User();
    user.setName("name");
    user.setUser("user");
    // 序列化
    byte[] serialize = Kson.serialize(user);
    // 反序列化
    User deserialize = Kson.deserialize(serialize, user.getClass());
    System.out.println(deserialize.getName());
    System.out.println(deserialize.getUser());
  }

}
```